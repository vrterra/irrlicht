# VRterra TCP API #

This document details the API available from the VRterra server.

## Connecting ##

To connect simply use regular TCP and connect to the IP of the server and the port the server is listening on. The default port is 24242.

## Network Packets ##

All packets are serialized with JSON and must also be deserialized with JSON. Use your favorite language's built-in JSON parser or find a library to do it for you. Most languages should have one or fifteen parsers available.

The following sections details each packet, how to identify it and what fields it contains or what fields it is supposed to contain.

Every packet contains the 'flag' field which identifies it. The flag field is how each packet is identified, the section titles will name the packet type and the flag value (in parenthesis). On the server side the flag is stored as an unsigned 32-bit int, but they currently don't exceed the value 5. All field-names in the JSON message are lower-case.

### Disconnect (0) ###

`Server <-> Client`

This packet only contains the 'flag' field. It signals a disconnection. After receiving or sending this no further communication should be attempted.

### Heightmap Request (1) ###

`Server <- Client`

This message  only contains the 'flag' field.

This message requests that the server sends the heightmap to the client.

### Heigthmap Send (2) ###

`Server -> Client`

This message is only sent after receiving the 'Heightmap Request' message. This message contains the requested heightmap.

Its fields are:

```
flag        : standard field
columns     : 32-bit signed integer which is the number of columns in the heightmap
rows        : 32-bit signed integer which is the number of rows in the heightmap
heightmap   : an array of the float values in the heightmap
```

Note that 'rows' is _not_ needed to define the heightmap, but it can be used for error checking. If the number of values in the heightmap does not match columns * rows then something is wrong.

### Marker Initialize (3) ###

`Server <- Client`

This message only contains the 'flag' field. Use this packet to create a new marker on the server.

The server will respond with a 'Marker Initialize Response' packet.

Note: there is currently no way to remove a marker.

### Marker Position Send (4) ###

`Server <- Client`

This message lets you set a new position for a marker.

Its fields are:

```
flag    : standard field
id      : 32-bit signed integer which is the id of the marker
x       : 32-bit signed integer which is the position on the x-axis
y       : 32-bit signed integer which is the position on the y-axis
```

### Marker Initialize Response (5) ###

`Server -> Client`

This message is sent as a response to the 'Marker Initialize' packet.

Its fields are:

```
flag    : standard field
id      : signed 32-bit id to use to address the marker
```

Use the 'id' received in this packet to address the marker in future 'Marker Position Send' packets.
