#include "EditorReceiver.h"

EditorReceiver::EditorReceiver()
{
    for (irr::u32 i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i)
    {
        m_KeyIsDown[i] = false;
    }
}

bool EditorReceiver::OnEvent(const irr::SEvent& event)
{
    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
        m_KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
    }

    return false;
}
