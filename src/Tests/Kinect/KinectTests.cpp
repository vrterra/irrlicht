#include <gtest/gtest.h>
#include <libfreenect/libfreenect.hpp>
#include <Kinect/Kinect.h>

/**
 * Minimal example of how to use Kinect.h to get depthmap from a kinect
 * Keep in mind that you actually need a Kinect plugged in to succeed this test
 * todo: create kinect flags in cmake, maybe
 * @author Nichlas Severinsen
 */
TEST(KinectTests, TestDepthBuffer)
{
    Freenect::Freenect freenect;
    VRterraFreenectDevice* device = &freenect.createDevice<VRterraFreenectDevice>(0);

    // Set format and start recording depth
    device->setDepthFormat(FREENECT_DEPTH_11BIT);
    device->startDepth();

    std::vector<uint16_t> buffer(640 * 480 * 3);
    while(!device->getDepth(buffer)){
        // dat busy wait
    }

    if (device->getDepth(buffer))
    {
        for (int i = 0; i < buffer.size(); i++)
        {
            if (buffer[i] < 0)
            {
                FAIL();
            }
        }
    }
    else
    {
        FAIL();
    }

    device->stopDepth();

    SUCCEED();
}
