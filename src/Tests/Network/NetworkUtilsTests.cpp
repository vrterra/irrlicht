#include <gtest/gtest.h>

#include <Network/NetworkUtils.h>
#include <Terrain/Terrain.h>

#include <numeric>

#include <enet/enet.h>

TEST(NetworkUtilsTests, TestComparePeers)
{
    ENetPeer a;
    a.address.host = 12;
    a.address.port = 245;
    
    ENetPeer b;
    b.address.host = 13;
    b.address.port = 245;
    
    ENetPeer c;
    c.address.host = 12;
    c.address.port = 245;
    
    ENetPeer d;
    d.address.host = 12;
    d.address.port = 246;
    
    ASSERT_FALSE(NetUtils::comparePeers(&a, &b));
    ASSERT_TRUE(NetUtils::comparePeers(&a, &c));
    ASSERT_FALSE(NetUtils::comparePeers(&a, &d));
    ASSERT_FALSE(NetUtils::comparePeers(&b, &c));
}

TEST(NetworkUtilsTests, TestApplyFullUpdate)
{
    Terrain terr{1,1};
    
    terrain::FullHeightmap hMap;
    hMap.New();
    hMap.set_columns(2);
    hMap.set_rows(2);
    auto mutHMap = hMap.mutable_heightmap();
    mutHMap->Resize(2*2, 0);
    std::iota(mutHMap->begin(), mutHMap->end(), 0.f);
    
    NetUtils::applyFullUpdate(&terr, &hMap);
    ASSERT_EQ(4, terr.getSize());
    ASSERT_EQ(0.f, terr.getValueAtIndex(0));
    ASSERT_EQ(1.f, terr.getValueAtIndex(1));
    ASSERT_EQ(2.f, terr.getValueAtIndex(2));
    ASSERT_EQ(3.f, terr.getValueAtIndex(3));
}
TEST(NetworkUtilsTests, TestExtractProtobufUnion)
{
    terrain::Union un;
    un.set_typeflag(TypeFlags::SERVER);
    un.set_flag(Flags::FULL_TERRAIN);

    auto tempArray = new uint8_t[un.ByteSize()];
    un.SerializeToArray(tempArray, un.ByteSize());
    
    auto packet = enet_packet_create(tempArray, un.ByteSize(), ENET_PACKET_FLAG_NO_ALLOCATE);
    auto content = NetUtils::extractProtobufUnion(packet);
    enet_packet_destroy(packet);

    ASSERT_NE(nullptr, content);
    ASSERT_EQ(TypeFlags::SERVER, content->typeflag());
    ASSERT_EQ(Flags::FULL_TERRAIN, content->flag());
}
