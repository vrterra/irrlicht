﻿#include <gtest/gtest.h>
#include <Utils/Container.h>

TEST(ContainerTests, TestRemove)
{
    std::vector<int> vec    = {1, 1, 1, 1, 2, 2, 2, 1, 1, 1};
    std::vector<int> expect = {2, 2, 2};

    Utils::remove(&vec, 1);
    ASSERT_EQ(expect.size(), vec.size());

    for (auto i = 0ull; i < expect.size(); i++)
    {
        ASSERT_EQ(expect.at(i), vec.at(i));
    }

    Utils::remove(&vec, 2);
    ASSERT_EQ(0, vec.size());
}
