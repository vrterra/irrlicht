﻿#include <gtest/gtest.h>
#include <Utils/Config.h>

TEST(ConfigTests, TestGetValue)
{
    std::string str = R"(
{
    "integer": 55,
    "boolean": false,
    "string": "hello",
    "double": 66.6
}
)";

    Utils::Config config;
    config.parse(str);

    auto integer = config.getValue<int>("integer");
    auto boolean = config.getValue<bool>("boolean");
    auto string = config.getValue<std::string>("string");
    auto doubleVal = config.getValue<double>("double");
    // Testing default constructor / standard value:
    auto standardDefault = config.getValue<double>("double2");
    auto expDefault = config.getValue<double>("double3", 24.2);

    EXPECT_EQ(55, integer);
    EXPECT_EQ(false, boolean);
    EXPECT_EQ("hello", string);
    EXPECT_EQ(66.6, doubleVal);
    // default constructor
    EXPECT_EQ(0, standardDefault);
    // explicit standard value
    EXPECT_EQ(24.2, expDefault);
}
