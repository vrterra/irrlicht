﻿#pragma once
#include "Flags.h"
#include "Network.h"
#include "terrain.pb.h"

#include <Terrain/Terrain.h>
#include <Utils/Error.h>

#include <algorithm>
#include <string>

/**
* This file contains a collection of free functions to use in the Network-related code
*
* @author Mårten Nordheim
*/

namespace NetUtils
{
    /**
    * Serializes an instance of terrain::Union and sends it to a peer
    * @param data the terrain::Union instance
    * @param network a pointer to an instance of network
    * @param peer a pointer to a peer
    */
    inline void sendProtobufPacket(const gsl::not_null<const terrain::Union*>& data,
                                   const gsl::not_null<const Network*>& network,
                                   const gsl::not_null<ENetPeer*>& peer)
    {
        auto tempArray = new uint8_t[data->ByteSize()];
        data->SerializeToArray(tempArray, data->ByteSize());
        network->sendData({tempArray, data->ByteSize()}, peer);
        delete[] tempArray;
    }

    /**
    * Extracts the terrain::Union object from a packet
    * @param packet a pointer to the packet
    * @return a pointer to the Union
    */
    inline gsl::owner<terrain::Union*> extractProtobufUnion(
        const gsl::not_null<const ENetPacket*>& packet)
    {
        auto content = new terrain::Union();
        if (!content->ParseFromArray(packet->data, gsl::narrow_cast<int>(packet->dataLength)))
        {
            DBG("Failed to extract union!");
            delete content;
            return nullptr;
        }
        return content;
    }

    /**
    * Apply delta changes to a terrain
    * @param terrain the terrain to apply the changes to
    * @param update the update to apply
    */
    inline void applyDeltaChanges(const gsl::not_null<Terrain*>& terrain,
                                  const gsl::not_null<const terrain::TerrainUpdate*>& update)
    {
        auto amountOfDeltaRanges = update->deltachange().size();
        std::map<size_t, float> deltaMap;

        for (auto i = 0; i < amountOfDeltaRanges; i++)
        {
            auto currentChange = update->deltachange().Get(i);
            auto index         = currentChange.index();
            auto delta         = currentChange.difference();

            deltaMap[index] += delta;
        }
        terrain->copyFromDeltaMap(deltaMap);
    }

    /**
    * Apply full terrain update to a terrain
    * @param terrain pointer to the terrain to work on
    * @param heightmap the full terrain packet
    */
    inline void applyFullUpdate(const gsl::not_null<Terrain*>& terrain,
                                const gsl::not_null<const terrain::FullHeightmap*>& heightmap)
    {
        // Resize the terrain
        terrain->setNewSize(heightmap->columns(), heightmap->rows());
        auto size = terrain->getSize();

        std::vector<float> buffer(size);
        std::copy(heightmap->heightmap().begin(), heightmap->heightmap().end(), buffer.begin());
        terrain->copyFromBuffer(buffer);
    }

    /**
    * Send the full terrain to a peer
    * @param terrain a pointer to the terrain
    * @param network a pointer an instance of network
    * @param peer a pointer to the receiving peer
    * @param typeFlag the type of network unit which is calling this function
    */
    inline void sendCompleteTerrain(const gsl::not_null<const Terrain*>& terrain,
                                    const gsl::not_null<const Network*>& network,
                                    const gsl::not_null<ENetPeer*>& peer,
                                    TypeFlags typeFlag)
    {
        terrain::Union packetContent;
        packetContent.set_flag(FULL_TERRAIN);
        packetContent.set_typeflag(typeFlag);

        packetContent.mutable_fullheightmap()->set_rows(
            gsl::narrow_cast<google::protobuf::uint32>(terrain->getHeight()));

        packetContent.mutable_fullheightmap()->set_columns(
            gsl::narrow_cast<google::protobuf::uint32>(terrain->getWidth()));

        auto heightmap = terrain->getHeightmap();
        // Set off enough space in the protobuf structure, and then copy in the terrain
        auto mutableProtoHeightmap = packetContent.mutable_fullheightmap()->mutable_heightmap();
        mutableProtoHeightmap->Resize(gsl::narrow_cast<int>(terrain->getSize()), 0);
        std::copy(heightmap->begin(), heightmap->end(), mutableProtoHeightmap->begin());

        sendProtobufPacket(&packetContent, network, peer);
    }

    /**
    * Only compares the address and port of the peer, not full equality
    * @param a a peer
    * @param b another peer
    * @return true if the address and port is the same, false otherwise
    */
    inline bool comparePeers(const gsl::not_null<const ENetPeer*>& a,
                             const gsl::not_null<const ENetPeer*>& b)
    {
        return a->address.host == b->address.host && a->address.port == b->address.port;
    }

    /**
    * Get a printable string from an address
    * @param address the address
    * @return printable string containing the address
    */
    inline std::string getIPString(const ENetAddress& address)
    {
        const auto IPV6_MAX_LEN = 46;  // ipv6 max length is 45 + '\0'
        char c[IPV6_MAX_LEN];

        if (enet_address_get_host_ip(&address, c, IPV6_MAX_LEN) == 0)
        {
            std::string str = c;
            return str;
        }

        // If we reach this point then we couldn't get the IP for some reason:
        return "ERROR-IP";
    }
}
