﻿#pragma once
// Need to include stdint here or the enums are undefined..
#include <stdint.h>

enum Flags : uint32_t
{
    CONNECT       = 0x0,
    FULL_TERRAIN  = 0x1,
    DELTA_CHANGES = 0x2
};

enum TypeFlags : uint32_t
{
    CLIENT = 0x0,
    SERVER = 0x1
};

enum TcpFlags : uint32_t
{
    // Connection related
    DISCONNECT = 0x0,
    // Heightmap related
    HEIGHTMAP_REQUEST = 0x1,
    HEIGHTMAP_SEND    = 0x2,
    // Marker related
    MARKER_INITIALIZE    = 0x3,
    MARKER_POSITION_SEND = 0x4,
    MARKER_INITIALIZE_RESPONSE = 0x5
};