#pragma once
#include <libfreenect/libfreenect.hpp>

#include <Utils/Error.h>

#include <iostream>
#include <vector>
#include <cmath>
#include <mutex>
#include <thread>

/**
 * A custom FreenectDevice class for device communication
 * This class includes minimal amount of functions; callback,
 * constructor, and getDepth which gives the depthmap.
 * @author Nichlas Severinsen
 */
class VRterraFreenectDevice : public Freenect::FreenectDevice
{
public:
    static const int KINECT_RES_WIDTH  = 640;
    static const int KINECT_RES_HEIGHT = 480;

    /**
     * Constructor for initialization
     */
    VRterraFreenectDevice(freenect_context* _ctx, int _index)
        : Freenect::FreenectDevice(_ctx, _index),
          m_currentBuffer(0),
          m_maxBuffers(10),
          m_buffer_depth(m_maxBuffers),
          m_new_depth_frame(false)
    {
        for (auto i = 0; i < m_buffer_depth.size(); i++)
        {
            m_buffer_depth[i].resize(KINECT_RES_HEIGHT * KINECT_RES_WIDTH);
        }
    }

    /**
     * Callback for depthmap
     */
    void DepthCallback(void* _depth, uint32_t timestamp)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        auto depth = static_cast<uint16_t*>(_depth);
        for (unsigned int i = 0; i < KINECT_RES_HEIGHT * KINECT_RES_WIDTH; i++)
        {
            m_buffer_depth[m_currentBuffer][i] = depth[i];
        }
        m_currentBuffer   = (m_currentBuffer + 1) % m_maxBuffers;
        m_new_depth_frame = true;
    }

    /**
     * Function for putting depthmap into an external buffer
     */
    bool getDepth(std::vector<uint16_t>& buffer)
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        if (m_new_depth_frame)
        {
            for (auto j = 0; j < m_buffer_depth.size(); j++)
            {
                for (auto i = 0; i < buffer.size(); i++)
                {
                    buffer[i] += m_buffer_depth[j][i];
                }
            }
            for (auto i = 0; i < buffer.size(); i++)
            {
                buffer[i] /= m_buffer_depth.size();
            }
            m_new_depth_frame = false;
            return true;
        }
        else
        {
            return false;
        }
    }

private:
    std::mutex m_mutex;
    int m_currentBuffer;
    int m_maxBuffers;
    std::vector<std::vector<uint16_t>> m_buffer_depth;
    bool m_new_depth_frame;
};
