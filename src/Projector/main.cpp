#include <Base/Base.h>
#include <Base/BaseReceiver.h>
#include <Kinect/Kinect.h>
#include <Server/Server.h>
#include <Terrain/Terrain.h>

#include <thread>
#include <Base/Projector.h>

void nect(Terrain* terrain, bool* running)
{
    if (!running)
    {
        return;
    }

    DBG("Starting to tread :)");
    // KINECT START
    std::vector<uint16_t> buffer(VRterraFreenectDevice::KINECT_RES_HEIGHT *
                                 VRterraFreenectDevice::KINECT_RES_WIDTH);
    std::vector<float> inverseBuffer(buffer.size());

    Freenect::Freenect freenect;
    VRterraFreenectDevice* device = &freenect.createDevice<VRterraFreenectDevice>(0);

    // Set format and start recording depth
    device->setDepthFormat(FREENECT_DEPTH_11BIT);
    device->startDepth();

    while (*running != false)
    {
        while (!device->getDepth(buffer))
        {
            // dat busy wait
        }

        for (int i = 0; i < buffer.size(); i++)
        {
            inverseBuffer[i] = 2048.f - buffer[i];
        }

        terrain->copyFromBuffer(inverseBuffer);
    }

    device->stopDepth();

    // KINECT END
    DBG("Endinging to tread :)");
}

int main(int argc, char** argv)
{
    bool use_kinect = true;
    if (argc > 1)
    {
        for (int i = 1; i < argc; i++)
        {
            if (strcmp(argv[i], "--no-kinect") == 0)
            {
                use_kinect = false;
            }
        }
    }
    std::vector<uint16_t> buffer(VRterraFreenectDevice::KINECT_RES_HEIGHT *
                                 VRterraFreenectDevice::KINECT_RES_WIDTH);

    auto terrain = std::make_shared<Terrain>(640, 480);

    Server server;
    server.initialize(terrain);

    terrain->addCallbackFunction("server", [&server](size_t, float, float, bool lastUpdate) {
       
        if (lastUpdate)
        {
            server.setTerrainUpdated();
        }
    });

    server.start();
    server.startTcpServer();

    Projector projector{"configuration/projector.json", terrain.get()};

    auto receiver =
        new BaseReceiver(projector.getTerrain(), projector.getDevice(), projector.getMouse());

    projector.setReceiver(receiver);

    auto running = true;
    if (!use_kinect)
    {
        running = false;
    }

    std::thread kinect(nect, terrain.get(), &running);

    projector.run();

    running = false;
    server.stop();
    kinect.join();

    return 0;
}
