file(GLOB SRC_FILES "*.h" "*.cpp")

add_executable(irrlicht-projector ${SRC_FILES})

target_link_libraries(irrlicht-projector Base Kinect Server)
install(TARGETS irrlicht-projector RUNTIME DESTINATION ${BIN_DIR} COMPONENT Projector)
