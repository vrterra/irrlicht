#pragma once
#include <Base/Base.h>

class Game : public Base
{
public:
    /**
    * Constructor for Game
    *
    * @param file the path of the configuration file to use
    * @param ourTerrain a pointer to an initialized instance of the Terrain class
    */
    Game(const gsl::cstring_span<>& file, Terrain* ourTerrain);

    /**
    * Destructor for Game
    */
    ~Game();
protected:
    /**
    * Set up the camera to use in the scene
    */
    void setupIrrlichtCamera();

    /**
    * Initialize the enet client and connect to the server specified in config
    */
    void initializeClient();

    /**
    * Update Game-specific elements
    *
    * @param deltaTime the time taken to complete one iteration of the game loop
    */
    void update(float deltaTime) override;

    /**
    * A chance to draw to the screen before it is displayed
    */
    void derivedDraw() override;

    /**
    * Force the terrain to be redrawn and, hackily, recalculate its normals
    *
    * @param terrain a pointer to irrlicht terrain scene node
    */
    void derivedTerrainUpdated(irr::scene::ITerrainSceneNode* terrain) override;

private:
    /**
    * Add a collision animator to the camera
    */
    void addCameraCollisionAnimator();

    irr::scene::ISceneNodeAnimatorCollisionResponse* m_collisionAnim;

    Client m_client;
};
