#pragma once

#include "Game.h"

#include <irrlicht.h>
#include <gsl.h>

/**
 * Receiver class for FPS Demo, aka "Game"
 *
 * @author Nichlas Severinsen
 */
class GameReceiver : public irr::IEventReceiver
{
public:
    /**
    * Constructor for GameReceiver
    *
    * @param game a pointer to an instance of Game
    */
    explicit GameReceiver(gsl::not_null<Game*> game);

    /**
    * Event handler
    * @param event event that comes in
    */
    bool OnEvent(const irr::SEvent& event) override;

private:
    Game* m_game;
};
