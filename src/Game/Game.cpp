#include "Game.h"

#include "GameShaderCallback.h"

Game::Game(const gsl::cstring_span<>& file, Terrain* ourTerrain)
    : Base(file, ourTerrain), m_collisionAnim(nullptr), m_client()
{
    // create a shader callback for irrlicht
    auto sCallback = new GameShaderCallback(getDevice());
    // set up the irrlicht terrain with the callback we created
    setupIrrlichtTerrain(sCallback);
    // Create a "light" (define a position for a light)
    auto lightPos = getTerrain()->getBoundingBox().getCenter();
    lightPos.Y += 10000;
    // tell the shader callback about the light's position
    sCallback->setLightPosition(lightPos);

    initializeClient();

    setupIrrlichtCamera();
}

Game::~Game()
{
    if (m_collisionAnim != nullptr)
    {
        m_collisionAnim->drop();
    }
}

void Game::setupIrrlichtCamera()
{
    auto center = getTerrain()->getBoundingBox().getCenter();
    center.X -= 975;
    irr::SKeyMap keyMap[4];
    keyMap[0].Action  = irr::EKA_MOVE_FORWARD;
    keyMap[0].KeyCode = irr::KEY_KEY_W;

    keyMap[1].Action  = irr::EKA_MOVE_BACKWARD;
    keyMap[1].KeyCode = irr::KEY_KEY_S;

    keyMap[2].Action  = irr::EKA_STRAFE_LEFT;
    keyMap[2].KeyCode = irr::KEY_KEY_A;

    keyMap[3].Action  = irr::EKA_STRAFE_RIGHT;
    keyMap[3].KeyCode = irr::KEY_KEY_D;

    m_camera = getSceneManager()->addCameraSceneNodeFPS(nullptr, 100.f, 1.2f, -1, keyMap, 4);

    m_camera->setPosition(irr::core::vector3df(20, 10, 20));
    m_camera->updateAbsolutePosition();
    m_camera->setTarget(center);
    m_camera->setFarValue(35000);

    addCameraCollisionAnimator();
}

void Game::addCameraCollisionAnimator()
{
    std::lock_guard<std::mutex> lock(m_renderMutex);

    auto selector = getSceneManager()->createTerrainTriangleSelector(getTerrain());
    getTerrain()->setTriangleSelector(selector);
    if (m_collisionAnim != nullptr)
    {
        m_camera->removeAnimator(m_collisionAnim);
        m_collisionAnim->drop();
    }
    m_collisionAnim =
        getSceneManager()->createCollisionResponseAnimator(selector,
                                                           m_camera,
                                                           irr::core::vector3df(60, 100, 60),
                                                           irr::core::vector3df(0, 0, 0),
                                                           irr::core::vector3df(0, 50, 0));
    selector->drop();
    m_camera->addAnimator(m_collisionAnim);
}

void Game::update(float)
{
    if (m_client.isConnected())
    {
        m_client.update(getTerrainClassInstance());
    }
}

void Game::derivedDraw()
{
    // currently unused
}

void Game::derivedTerrainUpdated(irr::scene::ITerrainSceneNode* terrain)
{
    // set the position with the current position (hacky, only way to make the visuals reflect the
    // change using the public interface)
    terrain->setPosition(terrain->getPosition());

    // set the scale with the current scale (hacky, only way to recalculate normals using the
    // public interface)
    terrain->setScale(terrain->getScale());

    addCameraCollisionAnimator();
}

void Game::initializeClient()
{
    auto targetHost = getConfig()->getValue<std::string>("HostAddress", "127.0.0.1");
    m_client.initialize();
    m_client.connect(targetHost);
}
