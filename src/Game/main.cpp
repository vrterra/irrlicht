﻿#include "Game.h"
#include "GameReceiver.h"

#include <Terrain/Terrain.h>

int main(int argc, char** argv)
{
    auto terrain = std::make_shared<Terrain>(640, 480);

    auto configFile = std::string("configuration/game.json");

    Game game{configFile, terrain.get()};

    auto receiver = GameReceiver(&game);
    game.setReceiver(&receiver);

    game.run();

    return 0;
}
