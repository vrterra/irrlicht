#include "GameReceiver.h"

GameReceiver::GameReceiver(gsl::not_null<Game*> game)
{
    m_game = game;
}

bool GameReceiver::OnEvent(const irr::SEvent& event)
{
    if (event.EventType == irr::EET_KEY_INPUT_EVENT && !event.KeyInput.PressedDown)
    {
        switch (event.KeyInput.Key)
        {
        case irr::KEY_ESCAPE:
        {
            m_game->getDevice()->closeDevice();
        }
        }
    }

    return false;
}
