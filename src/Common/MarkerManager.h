#pragma once

#include "Marker.h"

#include <atomic>
#include <vector>
#include <mutex>

/**
 * Singleton to hold and manage markers
 *
 * @author Mårten Nordheim 
 */
class MarkerManager
{
public:
    /**
    * Standard singleton "get instance" function
    *
    * @return a pointer to the single instance of this class
    */
    static MarkerManager* getInstance();

    // Delete copy-ctor
    MarkerManager(const MarkerManager&) = delete;
    // Delete copy assignment operator
    MarkerManager& operator=(const MarkerManager&) = delete;

    /**
    * Default destructor
    */
    ~MarkerManager() = default;

    /**
    * Create a new marker and get its ID
    *
    * @return the new id
    */
    int createNewMarker();

    /**
    * Get a marker based on its ID
    *
    * @param id the id
    * @return a pointer to the marker, or nullptr if not found
    */
    Marker* getMarker(const int& id);

    /**
    * Get the amount of markers stored in the manager
    *
    * @return the amount of markers in the manager
    */
    size_t size();
private:
    /**
    * Default constructor
    */
    MarkerManager() = default;

    std::vector<Marker> m_markers;
    int m_nextId = 0;
    std::mutex m_markerMutex;
};