﻿#pragma once

/**
* Struct to represent a marker
*
* @author Mårten Nordheim
*/
struct Marker
{
    explicit Marker(int id) : id(id), x(0), y(0), enabled(true)
    {
    }

    int id;
    int x, y;
    bool enabled;
};
