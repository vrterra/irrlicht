#include "MarkerManager.h"

#include <algorithm>

MarkerManager* MarkerManager::getInstance()
{
    static MarkerManager m {};
    return &m;
}

int MarkerManager::createNewMarker()
{
    std::lock_guard<std::mutex> lock(m_markerMutex);

    auto id = m_nextId;
    m_nextId++; // ready for the next marker
    m_markers.emplace_back(id);

    return id;
}

Marker* MarkerManager::getMarker(const int& id)
{
    std::lock_guard<std::mutex> lock(m_markerMutex);
    auto it =
        std::find_if(m_markers.begin(), m_markers.end(), [&id](Marker m) { return id == m.id; });
    if (it != m_markers.end())
    {
        return &*it;
    }
    return nullptr;
}

size_t MarkerManager::size()
{
    std::lock_guard<std::mutex> lock(m_markerMutex);
    return m_markers.size();
}
