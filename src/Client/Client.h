﻿#pragma once
#include <Network/Flags.h>
#include <Network/Network.h>
#include <Terrain/Terrain.h>

#include <gsl.h>

/**
* Networking client for the VrTerra project
*
* @author Mårten Nordheim
*/
class Client
{
public:
    /**
    * Constructor for Client
    */
    Client();

    /**
    * Destructor for Client
    */
    ~Client();

    /**
    * Initialize the client
    *
    * @param countdownAmount the amount of time that needs to pass before a new packet will be sent
    * (SANDBOX-type only)
    */
    void initialize(float countdownAmount = 1.f);

    /**
    * Connect to a server
    * @param host the hostname or ip to connect to
    */
    bool connect(gsl::cstring_span<> host);

    /**
    * Disconnect from a server
    */
    void shutdown() const;

    /**
    * Check for a packet and update terrain if any deltas has arrived
    *
    * @param terrain the terrain to edit if we received a packet regarding terrain
    * @param deltaTime the time passed since last time
    */
    void update(Terrain* terrain);

    /**
    * Check if you are connected to a server
    * @return true if you're connected to a server
    */
    bool isConnected() const;

    /**
    * Get the type which this client is set to
    *
    * @return the type
    */
    TypeFlags getType() const;

private:
    /**
    * Handle a received packet
    * @param packetEvent the event from which the packet was generated
    * @param terrain a pointer to the terrain to change if necessary
    */
    static void handlePacket(gsl::owner<ENetEvent*> packetEvent, Terrain* terrain);

    /**
    * Send delta changes to the server
    */
    void sendDeltas();

    gsl::owner<Network*> m_network;
    gsl::owner<ENetPeer*> m_server;

    // used to be able to have a SANDBOX client as well, but it's not used anymore
    TypeFlags m_type = CLIENT;
};
