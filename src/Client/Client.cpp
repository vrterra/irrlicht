﻿#include "Client.h"

#include <Network/Flags.h>
#include <Network/NetworkUtils.h>

#include <Utils/Error.h>

Client::Client() : m_network(nullptr), m_server(nullptr)
{
}

Client::~Client()
{
    shutdown();
    if (m_network != nullptr)
    {
        delete m_network;
    }
}

void Client::initialize(float countdownAmount)
{
    Network::initialize();

    m_network = new Network();
    m_network->startClient();

    DBG("Client initialized!");
}

bool Client::connect(gsl::cstring_span<> host)
{
    Expects(!isConnected());
    auto connectionSuccessful = m_network->connect(host, &m_server);

    if (connectionSuccessful)
    {
        // Need to send basic information about the client to the server
        terrain::Union data;
        data.set_flag(CONNECT);
        data.set_typeflag(m_type);
        NetUtils::sendProtobufPacket(&data, m_network, m_server);
        Ensures(isConnected());
    }
    return connectionSuccessful;
}

void Client::shutdown() const
{
    if (m_network != nullptr)
    {
        m_network->stop();
        m_network->shutdown();
    }
    Ensures(!isConnected());
}

void Client::handlePacket(gsl::owner<ENetEvent*> packetEvent, Terrain* terrain)
{
    auto content = NetUtils::extractProtobufUnion(packetEvent->packet);
    if (content == nullptr)
    {
        return;
    }
    auto finally = gsl::finally([&content]() { delete content; });
    DBG("Received a packet of size " << packetEvent->packet->dataLength);

    switch (static_cast<Flags>(content->flag()))
    {
    case FULL_TERRAIN:
    {
        Expects(content->has_fullheightmap());
        // Complete update of terrain
        NetUtils::applyFullUpdate(terrain, &content->fullheightmap());
        break;
    }
    case DELTA_CHANGES:
    {
        Expects(content->has_terrainupdate());
        NetUtils::applyDeltaChanges(terrain, &content->terrainupdate());
        break;
    }
    case CONNECT:
    {
        // not used on client
        break;
    }
    default:
    {
        DBG("Unknown flag " << content->flag() << " encountered!");
        break;
    }
    }

    enet_packet_destroy(packetEvent->packet);
}

void Client::update(Terrain* terrain)
{
    if (m_network == nullptr)
    {
        return;
    }

    auto packetEvent = m_network->receivePacket();

    if (packetEvent != nullptr)
    {
        if (packetEvent->type == ENET_EVENT_TYPE_DISCONNECT)
        {
            Expects(m_server == packetEvent->peer);
            m_server = nullptr;
        }
        else
        {
            handlePacket(packetEvent, terrain);
        }
        delete packetEvent;
    }
}

bool Client::isConnected() const
{
    return m_network != nullptr && m_network->isClientConnected();
}

TypeFlags Client::getType() const
{
    return m_type;
}
