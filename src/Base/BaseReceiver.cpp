#include "BaseReceiver.h"

#include <Utils/Error.h>
#include <iostream>

BaseReceiver::BaseReceiver(irr::scene::ITerrainSceneNode* terrain,
                           irr::IrrlichtDevice* device,
                           std::vector<irr::core::position2di>* mousePositions)
    : m_terrain(terrain), m_device(device), m_mousePositions(mousePositions)
{
}

bool BaseReceiver::OnEvent(const irr::SEvent& event)
{
    // mouse
    if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
    {
        if (event.MouseInput.Event == irr::EMIE_LMOUSE_PRESSED_DOWN)
        {
            if (m_mousePositions->size() < 4)
            {
                auto pos = irr::core::position2di(event.MouseInput.X, event.MouseInput.Y);
                m_mousePositions->push_back(pos);
            }
        }
    }

    // keyboard
    if (event.EventType == irr::EET_KEY_INPUT_EVENT && !event.KeyInput.PressedDown)
    {
        switch (event.KeyInput.Key)
        {
        case irr::KEY_KEY_2:
        {
            m_terrain->setMaterialFlag(irr::video::EMF_WIREFRAME,
                                       !m_terrain->getMaterial(0).Wireframe);
            m_terrain->setMaterialFlag(irr::video::EMF_POINTCLOUD, false);
            return true;
        }
        case irr::KEY_ESCAPE:
        {
            m_device->closeDevice();
            break;
        }
        case irr::KEY_KEY_C:
        {
            m_mousePositions->clear();
            break;
        }
        default:
            break;
        }
    }
    return false;
}
