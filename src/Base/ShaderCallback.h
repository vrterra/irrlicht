﻿#pragma once
#include <irrlicht.h>

/**
* Made for use with the irrlicht terrain, handles the callbacks that irrlicht generated for it
*
* @author Mårten Nordheim
*/
class ShaderCallback : public irr::video::IShaderConstantSetCallBack
{
public:
    /**
    * Constructor for ShaderCallback
    *
    * @param device a pointer to the irrlicht device of this application
    */
    explicit ShaderCallback(irr::IrrlichtDevice* device) : m_device(device)
    {
    }

    /**
    * Callback which Irrlicht uses
    *
    * Only sends in the MVP matrix
    */
    void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData) override
    {
        auto driver = services->getVideoDriver();

        auto worldViewProj = driver->getTransform(irr::video::ETS_PROJECTION);
        worldViewProj *= driver->getTransform(irr::video::ETS_VIEW);
        worldViewProj *= driver->getTransform(irr::video::ETS_WORLD);
        services->setVertexShaderConstant("worldViewProj",
                                          worldViewProj.pointer(),
                                          16);  // 16: mat4; 4x4
    }

protected:
    irr::IrrlichtDevice* m_device;
};