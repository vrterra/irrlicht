#pragma once
#include <Client/Client.h>
#include <Network/Flags.h>
#include <Utils/Config.h>

#include <gsl.h>
#include <irrlicht.h>

#include "ShaderCallback.h"
#include <Common/MarkerManager.h>
#include <mutex>

/**
 * Base class for interfacing with the Irrlicht engine
 * @author Nichlas Severinsen
 * @author Mårten Nordheim
 */
class Base
{
public:
    /**
    * Constructor
    *
    * @param file configuration filename
    * @param ourTerrain a pointer to an instance of our Terrain class
    */
    Base(gsl::cstring_span<> file, Terrain* ourTerrain);

    /**
     * Destructor
     */
    virtual ~Base();

    /**
    * Starts gameloop
    */
    void run();

    /**
     * Set receiver
     * @param receiver event receiver
     */
    void setReceiver(irr::IEventReceiver* receiver) const;

    /**
     * return pointer to terrain node/mesh
     */
    irr::scene::ITerrainSceneNode* getTerrain() const;

    /**
     * return pointer to camera node
     */
    irr::scene::ICameraSceneNode* getCamera() const;

    /**
     * Get pointer to device
     * @return the pointer to device
     */
    irr::IrrlichtDevice* getDevice() const;

    /**
    * Get the window size
    *
    * @return the current resolution/window size
    */
    irr::core::dimension2du getScreenDimensions() const;

protected:
    /**
    * Initialize and start the irrlicht window
    */
    void initializeIrrlichtWindow();

    /**
    * Set up the terrain (and shader)
    *
    * @param shaderCallback an instance of ShaderCallback which will be called to set constants in
    * the shader before rendering
    */
    void setupIrrlichtTerrain(ShaderCallback* shaderCallback);

    /**
    * Perform derivative-specific updates
    */
    virtual void update(float deltaTime) = 0;

    /**
    * Load a shader and return the material id
    *
    * @param shaderCallback an instance of ShaderCallback which will be called to set constants in
    * the shader before rendering
    * @return the material id
    */
    virtual int loadShader(ShaderCallback* shaderCallback) const;

    /**
    * Get a pointer to the scene manager
    *
    * @return the scene manager
    */
    irr::scene::ISceneManager* getSceneManager() const;

    /**
    * Get a pointer to the currently used Config instance
    *
    * @return the currently used instance of Config
    */
    Utils::Config* getConfig() const;

    /**
    * Get a pointer to irrlicht's driver
    *
    * @return the pointer to irrlicht's currently used video driver
    */
    irr::video::IVideoDriver* getDriver() const;

    /**
    * Get a pointer to the used instance of the Terrain Class
    *
    * @return a pointer to the Terrain class
    */
    Terrain* getTerrainClassInstance() const;

    /**
    * Let's derived classes draw something after the terrain was drawn but before it is displayed on
    * screen
    */
    virtual void derivedDraw() = 0;

    /**
    * Gets run when the irrlicht terrain mesh has gotten the final update in a batch
    *
    * @param terrain a pointer to the terrain node which was updated
    */
    virtual void derivedTerrainUpdated(irr::scene::ITerrainSceneNode* terrain) = 0;

    // Will be initialized and constructed in the child classes
    irr::scene::ICameraSceneNode* m_camera;
    // Need to be accessed from derived classes
    std::mutex m_renderMutex;

private:
    /**
    * Function to use with #Terrain::addCallbackFunction, updates Irrlicht terrain's mesh
    *
    * @param meshSize the size of the square mesh (length of x and z)
    * @param index the index in the Terrain-instance
    * @param newVal the new value at this index
    * @param isFinalUpdate true if this is the last known update
    */
    void syncMeshWithTerrainUpdates(size_t meshSize,
                                    size_t index,
                                    float newVal,
                                    bool isFinalUpdate);

    const float SCREEN_RATIO = 3.f / 4.f;

    irr::core::dimension2du m_screenDim;

    irr::scene::ITerrainSceneNode* m_terrain;

    irr::IrrlichtDevice* m_device;
    irr::video::IVideoDriver* m_driver;
    irr::scene::ISceneManager* m_smgr;
    irr::gui::IGUIEnvironment* m_guienv;
    Utils::Config* m_config;

    Terrain* m_ourTerrain;
};
