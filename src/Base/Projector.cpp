#include "Projector.h"
#include "ShaderCallback.h"

Projector::Projector(const gsl::cstring_span<>& file, Terrain* ourTerrain)
    : Base(file, ourTerrain),
      m_renderTexture(nullptr),
      m_textureWasMapped(false)
{
    m_mousePositions.reserve(4);

    setupIrrlichtTerrain(new ShaderCallback(getDevice()));

    setupIrrlichtCamera();
    initializeSelectionDisplayMesh();

    m_markerManager = MarkerManager::getInstance();
    // load a marker texture
    getDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
    auto img        = getDriver()->createImageFromFile("res/marker.png");
    m_markerTexture = getDriver()->addTexture("marker", img);
    getDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);
}

void Projector::setupIrrlichtCamera()
{
    auto center = getTerrain()->getBoundingBox().getCenter();

    // this really just is a large number, no particular meaning. The camera is orthographic so it
    // doesn't make a difference, it was chosen in case we wanted to draw something between the
    // terrain and the camera (we haven't done that though)
    auto largeNumber = 25000;

    m_camera =
        getSceneManager()->addCameraSceneNode(nullptr,
                                              irr::core::vector3df(center.X, largeNumber, center.Z),
                                              center);
    irr::core::matrix4 orthoMatrix;
    orthoMatrix.buildProjectionMatrixOrthoLH(m_projectionMatrixWidth,
                                             m_projectionMatrixHeight,
                                             1,
                                             largeNumber);
    m_camera->setProjectionMatrix(orthoMatrix, true);

    auto ourTerrain = getTerrainClassInstance();
    m_renderTexture = getDriver()->addRenderTargetTexture(
        irr::core::dimension2d<irr::u32>(static_cast<const irr::u32>(ourTerrain->getWidth()),
                                         static_cast<const irr::u32>(ourTerrain->getHeight())));
}

void Projector::renderToTexture()
{
    auto driver  = getDriver();
    auto terrain = getTerrain();
    driver->setRenderTarget(m_renderTexture, true, true, irr::video::SColor(255, 0, 0, 0));

    // set the terrain to visible in case it's invisible
    terrain->setVisible(true);
    // mutex lock scope
    {
        std::lock_guard<std::mutex> lock(m_renderMutex);
        getSceneManager()->drawAll();
    }
    // set the render target back to normal (screen)
    driver->setRenderTarget(nullptr, true, true, 0);

    // set it to invisible since we don't need to render this again during this frame
    terrain->setVisible(false);

    // if we haven't previously mapped the texture then we do it now
    if (!m_textureWasMapped)
    {
        m_textureWasMapped = true;
        mapTexture();
    }
}

void Projector::mapTexture()
{
    auto xratio = (static_cast<float>(m_renderTexture->getSize().Width) / getScreenDimensions().Width);
    auto yratio = (static_cast<float>(m_renderTexture->getSize().Height) / getScreenDimensions().Height);

    auto mousePositions = getMouse();

    auto upperLeft = mousePositions->at(0);
    upperLeft.X *= xratio;
    upperLeft.Y *= yratio;
    auto upperLeftVec =
        irr::core::vector2df{static_cast<float>(upperLeft.X) / m_renderTexture->getSize().Width,
                             static_cast<float>(upperLeft.Y) / m_renderTexture->getSize().Height};
    auto lowerLeft = mousePositions->at(1);
    lowerLeft.X *= xratio;
    lowerLeft.Y *= yratio;
    auto lowerLeftVec =
        irr::core::vector2df{static_cast<float>(lowerLeft.X) / m_renderTexture->getSize().Width,
                             static_cast<float>(lowerLeft.Y) / m_renderTexture->getSize().Height};
    auto lowerRight = mousePositions->at(2);
    lowerRight.X *= xratio;
    lowerRight.Y *= yratio;
    auto lowerRightVec =
        irr::core::vector2df{static_cast<float>(lowerRight.X) / m_renderTexture->getSize().Width,
                             static_cast<float>(lowerRight.Y) / m_renderTexture->getSize().Height};
    auto upperRight = mousePositions->at(3);
    upperRight.X *= xratio;
    upperRight.Y *= yratio;
    auto upperRightVec =
        irr::core::vector2df{static_cast<float>(upperRight.X) / m_renderTexture->getSize().Width,
                             static_cast<float>(upperRight.Y) / m_renderTexture->getSize().Height};

    m_meshNode->setMaterialTexture(0, m_renderTexture);

    m_meshBuffer->Vertices[0].TCoords = lowerLeftVec;
    m_meshBuffer->Vertices[1].TCoords = upperLeftVec;
    m_meshBuffer->Vertices[2].TCoords = upperRightVec;
    m_meshBuffer->Vertices[3].TCoords = lowerRightVec;
}

void Projector::initializeSelectionDisplayMesh()
{
    m_meshBuffer = new irr::scene::SMeshBuffer();
    m_meshBuffer->Vertices.set_used(4);
    m_meshBuffer->Indices.set_used(6);

    m_meshBuffer->Indices[0] = 0;
    m_meshBuffer->Indices[1] = 1;
    m_meshBuffer->Indices[2] = 2;
    m_meshBuffer->Indices[3] = 3;
    m_meshBuffer->Indices[4] = 0;
    m_meshBuffer->Indices[5] = 2;

    auto verticePosition = irr::core::vector3df(0, 0, 0);
    auto color           = irr::video::SColor(255, 255, 255, 255);
    // upper-left
    verticePosition.X -= 0.5f;
    verticePosition.Z -= 0.5f;
    m_meshBuffer->Vertices[0] = irr::video::S3DVertex(verticePosition,
                                                      irr::core::vector3df(0, 0, 0),
                                                      color,
                                                      irr::core::vector2df(0, 0));
    // lower-left
    verticePosition.Z += 1;
    m_meshBuffer->Vertices[1] = irr::video::S3DVertex(verticePosition,
                                                      irr::core::vector3df(0, 0, 0),
                                                      color,
                                                      irr::core::vector2df(0, 0));

    // lower-right
    verticePosition.X += 1;
    m_meshBuffer->Vertices[2] = irr::video::S3DVertex(verticePosition,
                                                      irr::core::vector3df(0, 0, 0),
                                                      color,
                                                      irr::core::vector2df(0, 0));

    // upper-right
    verticePosition.Z -= 1;
    m_meshBuffer->Vertices[3] = irr::video::S3DVertex(verticePosition,
                                                      irr::core::vector3df(0, 0, 0),
                                                      color,
                                                      irr::core::vector2df(0, 0));

    auto center = getTerrain()->getBoundingBox().getCenter();
    center.Y += 6000;

    auto mesh = new irr::scene::SMesh();
    mesh->addMeshBuffer(m_meshBuffer);
    m_meshNode =
        getSceneManager()->addMeshSceneNode(mesh,
                                            nullptr,
                                            -1,
                                            center,
                                            {0.f, 90.f, 0.f},
                                            {static_cast<float>(m_projectionMatrixWidth),
                                             1.f,
                                             static_cast<float>(m_projectionMatrixHeight)});

    mesh->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    m_meshNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
}

void Projector::updateMarkers()
{
    if (m_markerManager->size() > 0)
    {
        for (auto i = 0; i < m_markerManager->size(); i++)
        {
            auto marker     = m_markerManager->getMarker(i);
            auto markerSize = m_markerTexture->getSize();

            auto xratio = (static_cast<float>(m_renderTexture->getSize().Width) / getScreenDimensions().Width);
            auto yratio = (static_cast<float>(m_renderTexture->getSize().Height) / getScreenDimensions().Height);

            auto position =
                irr::core::vector2di{gsl::narrow_cast<int>(
                                         (marker->x - gsl::narrow_cast<int>(markerSize.Width)) *
                                         xratio),
                                     gsl::narrow_cast<int>(
                                         (marker->y - gsl::narrow_cast<int>(markerSize.Height)) *
                                         yratio)};

            if (m_markers.size() == i)
            {
                // Marker isn't already created as a GUI element, create it
                m_markers.emplace_back(
                    getSceneManager()->getGUIEnvironment()->addImage(m_markerTexture, position));
            }
            else
            {
                // move already existing marker
                m_markers[i]->setRelativePosition(position);
            }
        }
    }
}

void Projector::derivedDraw()
{
    if (m_mousePositions.size() < 4)
    {
        // Graphics for UV mapping:
        drawSelectionSquare();
    }
}

void Projector::drawSelectionSquare() const
{
    auto driver = getDriver();
    for (int i = 0; i < m_mousePositions.size(); i++)
    {
        // draw crosses on chosen points
        irr::core::position2di x1 = (m_mousePositions)[i] - irr::core::position2di(10, 0);
        irr::core::position2di x2 = (m_mousePositions)[i] + irr::core::position2di(10, 0);
        irr::core::position2di y1 = (m_mousePositions)[i] - irr::core::position2di(0, 10);
        irr::core::position2di y2 = (m_mousePositions)[i] + irr::core::position2di(0, 10);
        driver->draw2DLine(x1, x2);
        driver->draw2DLine(y1, y2);

        if (i > 0)
        {
            // draw lines between chosen points
            driver->draw2DLine((m_mousePositions)[i], (m_mousePositions)[i - 1]);
        }
        if (i == 3)
        {
            // draw line between last and first
            driver->draw2DLine((m_mousePositions)[i], (m_mousePositions)[0]);
        }
    }
}

void Projector::derivedTerrainUpdated(irr::scene::ITerrainSceneNode* terrain)
{
    // set the position with the current position (hacky, only way to make the visuals reflect the
    // changes in the mesh using the public interface)
    terrain->setPosition(terrain->getPosition());
    terrain->setScale(terrain->getScale());
}

void Projector::update(float deltaTime)
{
    if (m_renderTexture != nullptr)
    {
        // set as invisible
        m_meshNode->setVisible(false);
        if (m_mousePositions.size() == 4)
        {
            renderToTexture();
            // it has been rendered to now, set as visible
            m_meshNode->setVisible(true);
        }
        else if (!getTerrain()->isVisible() && m_mousePositions.size() < 4)
        {
            // in case the terrain was invisible and we didn't have enough points to make a
            // square (points are reset), we make it visible
            getTerrain()->setVisible(true);
            m_textureWasMapped = false;
        }
    }

    updateMarkers();
}

std::vector<irr::core::position2di>* Projector::getMouse()
{
    return &m_mousePositions;
}