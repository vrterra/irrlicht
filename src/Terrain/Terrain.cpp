#include "Terrain.h"

#include <Utils/Error.h>

#include <gsl_assert.h>

#include <iomanip>
#include <iostream>
#include <vector>

void Terrain::init()
{
    m_heightmap = std::make_shared<std::vector<float>>();
    m_heightmap->resize(static_cast<size_t>(m_terrainHeight * m_terrainWidth), 0.0f);
}

Terrain::Terrain()
{
    init();
}

Terrain::Terrain(size_t terrainWidth, size_t terrainHeight)
{
    m_terrainWidth  = terrainWidth;
    m_terrainHeight = terrainHeight;
    init();
}

std::shared_ptr<std::vector<float>> Terrain::getHeightmap() const
{
    return m_heightmap;
}

std::vector<float> Terrain::getHeightmapCopy() const
{
    return *m_heightmap;
}

size_t Terrain::getHeight() const
{
    return m_terrainHeight;
}

size_t Terrain::getSize() const
{
    return m_heightmap->size();
}

size_t Terrain::getWidth() const
{
    return m_terrainWidth;
}

void Terrain::setValueAtIndex(size_t index, float value, bool isFinalUpdate)
{
    auto heightmap = std::make_shared<std::vector<float>>(*m_heightmap);
    Expects(index < heightmap->size());
    auto oldValue       = (*heightmap)[index];
    (*heightmap)[index] = value;
    m_heightmap.swap(heightmap);

    notifyCallbackFunctions(index, value, oldValue, isFinalUpdate);
}

void Terrain::setNewSize(size_t newWidth, size_t newHeight)
{
    if (newWidth == m_terrainWidth && newHeight == m_terrainHeight)
    {
        return;
    }

    auto heightmap = std::make_shared<std::vector<float>>(*m_heightmap);
    auto newSize   = heightmap->size();

    auto oldWidth  = m_terrainWidth;
    m_terrainWidth = newWidth;
    auto widthDiff = gsl::narrow_cast<int>(newWidth) - gsl::narrow_cast<int>(oldWidth);
    newSize += widthDiff * m_terrainHeight;

    auto oldHeight  = m_terrainHeight;
    m_terrainHeight = newHeight;
    auto heightDiff = static_cast<int>(newHeight) - static_cast<int>(oldHeight);
    newSize += heightDiff * m_terrainWidth;

    heightmap->resize(newSize);
    m_heightmap.swap(heightmap);
}

void Terrain::addCallbackFunction(std::string key, CallbackFunc func)
{
    m_callbackFunctionMap[key] = func;
}

float Terrain::getValueAtIndex(size_t index) const
{
    auto heightmap = m_heightmap;
    Expects(index < heightmap->size());
    return (*heightmap)[index];
}

void Terrain::removeCallbackFunction(std::string key)
{
    auto entry = m_callbackFunctionMap.find(key);
    if (entry != m_callbackFunctionMap.end())
    {
        m_callbackFunctionMap.erase(entry);
    }
}

void Terrain::copyFromBuffer(const std::vector<float>& buffer)
{
    Expects(buffer.size() <= getSize());

    // Copy in the current heightmap (COW)
    auto heightmap = std::make_shared<std::vector<float>>(*m_heightmap);

    std::copy(buffer.begin(), buffer.end(), heightmap->begin());
    // Swap the current heightmap with our edited one (heightmap is now the old version of
    // m_heightmap)
    m_heightmap.swap(heightmap);
    // Copy the shared pointer to the current heightmap
    auto newHeightmap = m_heightmap;
    for (auto i = 0; i < buffer.size(); i++)
    {
        notifyCallbackFunctions(i, (*newHeightmap)[i], (*heightmap)[i], i == buffer.size() - 1);
    }
}

void Terrain::copyFromDeltaMap(const std::map<size_t, float>& deltaMap)
{
    // Copy in the current heightmap (COW)
    auto heightmap = std::make_shared<std::vector<float>>(*m_heightmap);

    for (auto keyValue : deltaMap)
    {
        (*heightmap)[keyValue.first] += keyValue.second;
    }
    // Swap the heightmap with the edited one
    m_heightmap.swap(heightmap);
    // Get a shared pointer to the new one
    auto newHeightmap = m_heightmap;

    // Notify callback functions about changes
    auto i    = 0;
    auto last = deltaMap.size() - 1;
    for (auto keyValue : deltaMap)
    {
        auto index = keyValue.first;
        notifyCallbackFunctions(index, (*newHeightmap)[index], (*heightmap)[index], i == last);
        i++;
    }
}

inline void Terrain::notifyCallbackFunctions(size_t index,
                                             float value,
                                             float oldValue,
                                             bool isFinalUpdate)
{
    if (value == oldValue && !isFinalUpdate)
    {
        return;
    }

    for (const auto& callback : m_callbackFunctionMap)
    {
        callback.second(index, value, oldValue, isFinalUpdate);
    }
}

Terrain::Terrain(size_t terrainWidth, size_t terrainHeight, std::vector<float> heightmap)
{
    Expects(terrainWidth * terrainHeight == heightmap.size());

    m_terrainWidth  = terrainWidth;
    m_terrainHeight = terrainHeight;
    m_heightmap     = std::make_shared<std::vector<float>>(heightmap);
}
