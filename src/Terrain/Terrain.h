#pragma once

#include <functional>
#include <map>
#include <memory>
#include <vector>

class TerrainRenderComponent;

// The way callbacks should be structured
typedef std::function<void(size_t index, float newVal, float oldVal, bool lastUpdate)> CallbackFunc;

/**
 * Shared terrain library, defines the terrain
 * @author Nichlas Severinsen
 * @author Mårten Nordheim
 */
class Terrain
{
public:
    /**
     * Constructor for terrain, initializes m_heightmap with zeros
     */
    Terrain();

    /**
     * Optional constructor with width and height for the terrain
     * @param terrainWidth the width of the terrain
     * @param terrainHeight the height of the terrain
     */
    Terrain(const size_t terrainWidth, const size_t terrainHeight);

    /**
     * Optional constructor with width, height, and heightmap (if loaded from elsewhere)
     * @param terrainWidth the width of the terrain
     * @param terrainHeight the height of the terrain
     * @param heightmap the heightmap of the terrain
     */
    Terrain(size_t terrainWidth, size_t terrainHeight, std::vector<float> heightmap);

    /**
     * Destructor for terrain
     */
    ~Terrain() = default;

    /**
     * Returns the heightmap of the terrain
     *
     * @see #getHeightmapCopy
     * @return The terrain heightmap
     */
    std::shared_ptr<std::vector<float>> getHeightmap() const;

    /**
    * Get a copy of the heightmap (return by value)
    *
    * @see #getHeightmap
    * @return a copy of the terrain heightmap
    */
    std::vector<float> getHeightmapCopy() const;

    /**
     * Returns size
     * @return the size
     */
    size_t getSize() const;

    /**
     * Returns width
     * @return the width
     */
    size_t getWidth() const;

    /**
     * Returns height
     * @return the height
     */
    size_t getHeight() const;

    /**
    * Set the value at an index to a specified value, controls access to the heightmap
    * @param index the index
    * @param value the new value
    * @param isFinalUpdate is this the final update?
    */
    void setValueAtIndex(size_t index, float value, bool isFinalUpdate);

    /**
    * Set the terrain to a new size, will likely invalidate and skew the current terrain
    * @param newWidth the new width
    * @param newHeight the new height
    */
    void setNewSize(size_t newWidth, size_t newHeight);

    /**
    * Add a function to be called whenever the heightmap is updated.
    * Call #removeCallbackFunction(std::string) with the same key once you are done.
    * @param key the key, used to index the function
    * @param func the function to call, must accept a size_t and a float
    */
    void addCallbackFunction(std::string key, CallbackFunc func);

    /**
    * Get the value at an index
    * @param index the index
    * @return the value at the specified index
    */
    float getValueAtIndex(size_t index) const;

    /**
    * Remove a callback function. If the function is not found no action will be taken.
    * @param key the key of the function you added earlier
    */
    void removeCallbackFunction(std::string key);

    /**
    * Copies the information from the passed-in buffer into the terrain's heightmap
    *
    * @param buffer the buffer you want to copy in, make sure its "width" is the same as the
    * terrain's
    */
    void copyFromBuffer(const std::vector<float>& buffer);

    /**
    * Copies in deltas from a map, passed in as an argument where size_t is the index and float is
    * the delta
    *
    * @param deltaMap the map of deltas
    */
    void copyFromDeltaMap(const std::map<size_t, float>& deltaMap);

private:
    /**
    * Resizes m_heightmap and fills it with zeros
    */
    void init();

    /**
    * Helper-function to call the callback functions
    *
    * @param index the index
    * @param value the new value
    * @param oldValue the old value
    * @param isFinalUpdate is the update final or not
    */
    inline void notifyCallbackFunctions(size_t index,
                                        float value,
                                        float oldValue,
                                        bool isFinalUpdate);

    std::shared_ptr<std::vector<float>> m_heightmap;
    size_t m_terrainWidth   = 10;  // X value height
    size_t m_terrainHeight  = 10;  // Y value height
    size_t m_terrainBreadth = 10;  // distance between vertices

    std::map<std::string, CallbackFunc> m_callbackFunctionMap;
};