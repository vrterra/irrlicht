#include "Error.h"

#include <iostream>
#include <sstream>
#include <string>

namespace Utils
{
    void logToScreen(std::string file, int line, std::string str)
    {
        std::stringstream ss;
        ss << file << " (" << line << "):" << std::endl << str.data() << std::endl;
        std::cerr << ss.str();
    }
}
