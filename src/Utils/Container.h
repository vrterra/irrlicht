﻿#pragma once

namespace Utils
{
 
    /**
    * Remove any occurrence of an object from a vector
    * @param container a pointer to the vector to work on
    * @param object the object to compare against (and remove)
    * @tparam T the object's type, must have a defined equality operator
    */
    template<class T>
    void remove(std::vector<T>* const container, const T object)
    {
        for (auto i = 0ull; i < container->size(); i++)
        {
            if (container->at(i) == object)
            {
                container->erase(container->begin() + i);
                i--;
            }
        }
    }

}