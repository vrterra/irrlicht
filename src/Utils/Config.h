﻿#pragma once

#include <gsl.h>

#include "IO.h"
#include <json.hpp>

namespace Utils
{
    using json = nlohmann::json;
    /**
    * A class for configuration files
    * @author Mårten Nordheim
    */
    class Config
    {
    public:
        /**
        * Constructor for Config
        */
        Config() = default;

        /**
        * Default destructor for Config
        */
        ~Config() = default;

        /**
        * Deleted copy constructor
        */
        Config(const Config&) = delete;
        /**
        * Deleted copy assignment operator
        */
        Config& operator=(const Config&) = delete;

        /**
        * Load a configuration file
        * @param path the path to the file
        */
        void load(gsl::cstring_span<> path)
        {
            m_json = json::parse(fileToString(path));
        }

        /**
        * Parse raw json (for testing)
        * @param jsonData the json string
        */
        void parse(gsl::cstring_span<> jsonData)
        {
            m_json = json::parse(jsonData.data());
        }

        /**
        * Get any value out of the config file
        * @param key the key whose value we should retrieve
        * @param defaultValue the default value to return if not found
        * @tparam T the type to load and return
        */
        template <typename T>
        T getValue(const gsl::cstring_span<>& key, T defaultValue = {})
        {
            auto val = m_json.find(key.data());
            if (val != m_json.end())
            {
                return val.value();
            }
            return defaultValue;
        }

        /**
        * Get the underlying json object
        * @return the json object
        */
        nlohmann::json& getJson()
        {
            return m_json;
        }

    private:
        nlohmann::json m_json;
    };
}
