﻿#include <Server/Server.h>

#include <iostream>
#include <sstream>

/**
* Entry-point
* @return exit code
*/
int main()
{
    Server server;
    server.initialize();
    server.start();
    server.startTcpServer();

    char c;
    do
    {
        std::cin >> c;
    } while (c != 'q');

    std::stringstream ss;
    ss << "Shutting down, please wait..." << std::endl;
    std::cout << ss.str();
    server.stop();
    return 0;
}
