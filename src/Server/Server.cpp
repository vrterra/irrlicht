﻿#include "Server.h"

#include <Network/Flags.h>
#include <Network/Network.h>
#include <Network/NetworkUtils.h>
#include <Network/terrain.pb.h>
#include <Utils/Config.h>
#include <Utils/Container.h>
#include <Utils/Error.h>

#include <algorithm>

Server::Server()
    : m_terrain(nullptr),
      m_port(0),
      m_waitTime(0),
      m_terrainHasUpdated(false),
      m_maxTerrainBroadcastFreq(0),
      m_broadcastTimer(0),
      m_thread(nullptr),
      m_tcpThread(nullptr),
      m_tcpServerInstance(nullptr)
{
}

Server::~Server()
{
    if (m_tcpThread != nullptr)
    {
        delete m_tcpThread;
        if (m_tcpServerInstance != nullptr)
        {
            delete m_tcpServerInstance;
        }
    }
    if (m_thread != nullptr)
    {
        delete m_thread;
    }
}

void Server::initialize(std::shared_ptr<Terrain> terrain)
{
    Network::initialize();

    Utils::Config config;
    config.load("configuration/server.json");

    m_port     = config.getValue<uint16_t>("ListenPort", PORT_NUMBER);
    m_waitTime = config.getValue<uint32_t>("WaitTime", 0);

    m_maxTerrainBroadcastFreq = config.getValue<float>("BroadcastFrequency", 5.0f);
    m_broadcastTimer          = m_maxTerrainBroadcastFreq;

    if (terrain != nullptr)
    {
        m_terrain = terrain;
    }
    else
    {
        auto terrainWidth  = config.getValue<uint32_t>("TerrainWidth", 10);
        auto terrainHeight = config.getValue<uint32_t>("TerrainHeight", 10);
        m_terrain          = std::make_shared<Terrain>(terrainWidth, terrainHeight);
    }

    DBG("Initialized!");
}

void Server::start()
{
    m_network.startServer(m_port);
    m_thread = new std::thread{&Server::listenLoop, this};
}

void Server::startTcpServer()
{
    m_tcpServerInstance = new TcpServer(m_ioService, m_port, m_terrain);
    m_tcpThread         = new std::thread([this]() -> void { m_ioService.run(); });
}

void Server::setTerrainUpdated()
{
    m_terrainHasUpdated = true;
}

void Server::broadcastTerrain()
{
    decltype(m_clients) copy;
    // mutex lock scope
    {
        std::lock_guard<std::mutex> lock(m_clientsLock);
        copy = m_clients;
    }
    for (auto& client : copy)
    {
        NetUtils::sendCompleteTerrain(m_terrain.get(), &m_network, client, SERVER);
    }
}

void Server::stop()
{
    m_network.stop();
    m_thread->join();
    m_network.shutdown();

    if (m_tcpServerInstance != nullptr && m_tcpServerInstance->isRunning())
    {
        stopTcpServer();
    }
}

void Server::stopTcpServer()
{
    m_tcpServerInstance->stop();

    // In case there are no TCP connection while shutting down it'll wait forever.. to avoid this we
    // stop the asio::io_service instance after 10 seconds
    std::thread timeout([this] {
        for (auto i = 0; i < 10 && !m_ioService.stopped(); i++)
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }

        if (!m_ioService.stopped())
        {
            m_ioService.stop();
        }
    });

    m_tcpThread->join();
    timeout.join();
}

void Server::listenLoop()
{
    std::chrono::system_clock clock;
    auto then = clock.now();
    while (m_network.isRunning())
    {
        auto now   = clock.now();
        auto delta = std::chrono::duration_cast<std::chrono::duration<float>>(now - then);
        then       = now;
        m_broadcastTimer -= delta.count();

        auto packetEvent = m_network.receivePacket(m_waitTime);

        if (packetEvent != nullptr)
        {
            auto deletePacketEvent = gsl::finally([&packetEvent] { delete packetEvent; });
            auto destroyPacket =
                gsl::finally([packetEvent] { enet_packet_destroy(packetEvent->packet); });

            // Need to handle disconnection..
            if (packetEvent->type == ENET_EVENT_TYPE_DISCONNECT)
            {
                std::lock_guard<std::mutex> lock(m_clientsLock);
                Utils::remove(&m_clients, packetEvent->peer);
                continue;
            }

            DBG("Received a packet of size " << packetEvent->packet->dataLength);
            auto content = NetUtils::extractProtobufUnion(packetEvent->packet);

            if (content == nullptr)
            {
                continue;
            }

            auto finally = gsl::finally([&content]() { delete content; });

            switch (static_cast<Flags>(content->flag()))
            {
            case CONNECT:
            {
                // Peer connected, needs initial terrain
                NetUtils::sendCompleteTerrain(m_terrain.get(),
                                              &m_network,
                                              packetEvent->peer,
                                              SERVER);
                // mutex lock scope
                {
                    std::lock_guard<std::mutex> lock(m_clientsLock);
                    m_clients.push_back(packetEvent->peer);
                }
                break;
            }
            case FULL_TERRAIN:
            {
                // not used on server
                break;
            }
            case DELTA_CHANGES:
            {
                // not used on server
                break;
            }
            default:
            {
                DBG("Reached default branch of switch with flag " << content->flag() << "!");
                break;
            }
            }
        }

        if (m_terrainHasUpdated && m_broadcastTimer <= 0)
        {
            m_terrainHasUpdated = false;
            m_broadcastTimer    = m_maxTerrainBroadcastFreq;
            broadcastTerrain();
        }
    }
}

/****************
*
* TCP server-related implementation...
*
****************/
// initializing static variables..
bool Server::TcpServer::m_running             = true;
MarkerManager* Server::TcpServer::m_markerMan = MarkerManager::getInstance();

Server::TcpServer::TcpServer(asio::io_service& io_service,
                             uint16_t port,
                             std::shared_ptr<Terrain> terrain)
    : m_acceptor(io_service, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port)),
      m_socket(io_service),
      m_terrain(terrain)
{
    doAccept();
}

void Server::TcpServer::stop()
{
    m_running = false;

    for (auto& thread : m_tcpClientThreads)
    {
        thread.join();
    }
}

bool Server::TcpServer::isRunning()
{
    return m_running;
}

bool Server::TcpServer::checkAsioError(asio::error_code& error, const size_t& bytesRead)
{
    if (error.value() != 0)
    {
        // some error occurred..

        if (error.value() != asio::error::eof || bytesRead == 0)
        {
            // either we have nothing to process (eof and bytesRead==0) or an error
            // occurred and we don't want to proceed anyway
            return false;
        }
    }
    return true;
}

asio::error_code Server::TcpServer::asioReadUntilSize(asio::ip::tcp::socket& socket,
                                                      asio::streambuf& streamBuffer,
                                                      bool* isConnected,
                                                      const size_t& requiredSize)
{
    asio::error_code error;

    while (streamBuffer.size() < requiredSize && (*isConnected))
    {
        // read until we have the entire message
        auto bytesRead = asio::read(socket, streamBuffer, error);
        if (!checkAsioError(error, bytesRead))
        {
            return error;
        }
        else if (error.value() == asio::error::eof && streamBuffer.size() < requiredSize)
        {
            *isConnected = false;
            return error;
        }
    }
    return error;
}

asio::error_code Server::TcpServer::readAsioMessage(asio::ip::tcp::socket& socket,
                                                    asio::streambuf& streamBuffer,
                                                    std::string* output,
                                                    bool* isConnected)
{
    asio::error_code error;

    auto searchString       = std::string("\r\n");
    auto containsNextHeader = false;
    if (streamBuffer.size() > 0)
    {
        // There is data left over, we need to see if it contains a header

        auto data  = streamBuffer.data();
        auto begin = asio::buffers_begin(data);
        auto end   = asio::buffers_end(data);

        auto result        = std::search(begin, end, searchString.begin(), searchString.end());
        containsNextHeader = (result != end);
    }

    if (!containsNextHeader)
    {
        auto bytesRead = asio::read_until(socket, streamBuffer, searchString, error);
        if (!checkAsioError(error, bytesRead))
        {
            return error;
        }

        if (error.value() == asio::error::eof)
        {
            // We got an eof error while reading, so we can't read anymore (but we still got some
            // information to process)
            *isConnected = false;
        }
    }

    // The full message's size is written in the "header", need to retrieve it:
    size_t messageSize;
    std::istream bufferStream(&streamBuffer);
    bufferStream >> messageSize;
    if (messageSize == 0)
    {
        throw std::domain_error("No message length header");
    }
    bufferStream.ignore(searchString.size());  // ignore '\r' and '\n'

    if (!(*isConnected) && streamBuffer.size() < messageSize)
    {
        // We got an eof while reading earlier and we're missing some of the message so we can't
        // continue
        return error;
    }

    auto origSize = streamBuffer.size();
    // Read more data into streamBuffer until its size is equal or greater than messageSize
    error          = asioReadUntilSize(socket, streamBuffer, isConnected, messageSize);
    auto bytesRead = streamBuffer.size() - origSize;
    if (!checkAsioError(error, bytesRead))
    {
        return error;
    }

    // move the message into 'output'
    auto str = new char[messageSize + 1];
    bufferStream.get(str, messageSize + 1);
    *output = std::string(str);
    delete[] str;

    return error;
}

void Server::TcpServer::sendJsonMessage(asio::ip::tcp::socket& socket,
                                        const nlohmann::json& message,
                                        asio::error_code& error)
{
    auto messageContent = message.dump();

    // prepend the string size (for error checking and knowing how much to read)
    messageContent = std::to_string(messageContent.size()) + "\r\n" + messageContent;

    auto buffer = asio::buffer(messageContent,
                               messageContent.size() * sizeof(decltype(messageContent.front())));

    asio::write(socket, buffer, error);
}

void Server::TcpServer::handleTcpMessage(asio::ip::tcp::socket& socket,
                                         const nlohmann::json& incomingMessage,
                                         Terrain* terrain,
                                         bool* isConnected,
                                         asio::error_code& error)
{
    auto flag = incomingMessage.at("flag").get<int>();

    switch (static_cast<TcpFlags>(flag))
    {
    case DISCONNECT:
    {
        *isConnected = false;
        return;
    }
    case HEIGHTMAP_REQUEST:
    {
        nlohmann::json message;
        message["flag"]      = static_cast<int>(TcpFlags::HEIGHTMAP_SEND);
        message["columns"]   = static_cast<int>(terrain->getWidth());
        message["rows"]      = static_cast<int>(terrain->getHeight());
        message["heightmap"] = *terrain->getHeightmap();
        sendJsonMessage(socket, message, error);
        break;
    }
    case HEIGHTMAP_SEND:
    {
        DBG("HEIGHTMAP_SEND is unused on server-side");
        break;
    }
    case MARKER_INITIALIZE:
    {
        // received request to add a marker

        // Send a response
        nlohmann::json message;
        message["flag"] = static_cast<int>(TcpFlags::MARKER_INITIALIZE_RESPONSE);
        message["id"]   = m_markerMan->createNewMarker();
        sendJsonMessage(socket, message, error);
        break;
    }
    case MARKER_INITIALIZE_RESPONSE:
    {
        DBG("MARKER_INITIALIZE_RESPONSE is unused on server-side");
        break;
    }
    case MARKER_POSITION_SEND:
    {
        // received a new position for a marker
        auto id     = incomingMessage.at("id").get<int>();
        auto marker = m_markerMan->getMarker(id);
        if (marker != nullptr)
        {
            marker->x = incomingMessage.at("x").get<int>();
            marker->y = incomingMessage.at("y").get<int>();
        }
        else
        {
            DBG("Got a message about a marker which does not exist:" << id);
            // @todo maybe notify the client that the marker doesn't exist?
        }

        break;
    }
    default:
    {
        throw std::range_error("Uncovered flag was encountered");
    }
    }  // switch end
}

void Server::TcpServer::handleConnection(std::shared_ptr<Terrain> terrain,
                                         asio::ip::tcp::socket socket)
{
    auto isConnected = true;
    asio::error_code error;
    asio::streambuf streamBuffer;

    while (socket.is_open() && isConnected)
    {
        if (!m_running)
        {
            // currently shutting down, send a disconnect message
            nlohmann::json disconnectMessage{{"flag", static_cast<int>(TcpFlags::DISCONNECT)}};
            sendJsonMessage(socket, disconnectMessage, error);
            isConnected = false;
            continue;
        }

        if (socket.available() == 0 && streamBuffer.size() <= 0)
        {
            // No information is available and no information left over to process
            // sleep a bit and then check again
            std::this_thread::sleep_for(std::chrono::milliseconds{150});
            continue;
        }

        std::string jsonString;
        try
        {
            error = readAsioMessage(socket, streamBuffer, &jsonString, &isConnected);
            if (!checkAsioError(error))
            {
                isConnected = false;
                continue;
            }
        }
        catch (std::domain_error e)
        {
            DBG("Error occurred while reading from TCP socket: " << std::endl << e.what());
            return;
        }

        if (jsonString.empty())
        {
            DBG("json string was empty, continuing");
            continue;
        }

        auto incomingMessage = nlohmann::json::parse(jsonString);
        handleTcpMessage(socket, incomingMessage, terrain.get(), &isConnected, error);
    }
}

void Server::TcpServer::doAccept()
{
    // accept connection to the socket asynchronously
    m_acceptor.async_accept(m_socket, [this](std::error_code errorCode) {
        if (!errorCode)
        {
            DBG("TCP connection received!");

            // Start the lambda in a thread and put it in a container
            m_tcpClientThreads.emplace_back(handleConnection, m_terrain, std::move(m_socket));
        }

        if (m_running)
        {
            // If we're not currently shutting down then we call doAccept, which sets up a new
            // asynchronous accept..
            doAccept();
        }
    });
}
