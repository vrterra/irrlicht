# Author: Mårten "Morten242" Nordheim

# It's probably easiest to just define IRRLICHT_HOME as an environment variable

FIND_PATH(IRRLICHT_INCLUDE_DIR irrlicht.h
    HINTS
    $ENV{IRRLICHT_HOME}/include
    PATHS
    /usr/include
    /usr/include/irrlicht
    /usr/local/include
    /usr/local/include/irrlicht
    /sw/include
    )

IF (MSVC)
    # Lookup the 64 bit libs on x64
    IF(CMAKE_SIZEOF_VOID_P EQUAL 8)
        FIND_LIBRARY(IRRLICHT_LIBRARY Irrlicht
            HINTS
            $ENV{IRRLICHT_HOME}/lib
            PATH_SUFFIXES
            Win64-visualStudio # This is the standard one
            x64
            Win64
            )
    # 32 bit on x86
    ELSE(CMAKE_SIZEOF_VOID_P EQUAL 8)
        FIND_LIBRARY(IRRLICHT_LIBRARY Irrlicht
            HINTS
            $ENV{IRRLICHT_HOME}/lib
            PATH_SUFFIXES
            Win32-visualStudio # This is the standard one
            x86
            Win32
            )
    ENDIF(CMAKE_SIZEOF_VOID_P EQUAL 8)
ELSE(MSVC)
    # Not visual studio..
    FIND_LIBRARY(IRRLICHT_LIBRARY Irrlicht
        HINTS
        $ENV{IRRLICHT_HOME}/lib
        PATH_SUFFIXES
        Linux
        MacOSX
        Win32-gcc        
        PATHS
        /usr/lib
        /usr/local/lib
        /opt/local/lib
        /sw/lib
        )
ENDIF(MSVC)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(IRRLICHT REQUIRED_VARS IRRLICHT_LIBRARY IRRLICHT_INCLUDE_DIR)
