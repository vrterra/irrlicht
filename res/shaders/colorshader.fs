#version 330 compatibility

in vec4 position;
in float dotProduct;

out vec4 color;

void main(void)
{
    float top = 2047; // the max is ((2^11) - 1) -- the kinect's depth buffer is 11 bits
    float redStart = 1500.0;
    float bottom = 1000; // approximate guess

    float level = (position.y-bottom) / (redStart-bottom);
    color = mix(vec4(0, 0.2, 0, 0), vec4(0, 1, 0, 0), level);

    if (mod(position.y, 2) - 0.6 < 0.0001 && dotProduct < 0.9999)
    {
        color = vec4(0.2, 0.2, 0.2, 0);
    }
}
