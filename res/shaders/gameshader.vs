#version 330 compatibility

uniform mat4 inverseWorld;
uniform mat4 worldViewProj;
uniform vec3 lightPosition;
uniform mat4 transposedWorld;


out vec4 position;

void main(void)
{
    position = gl_Vertex;
     gl_Position = worldViewProj * gl_Vertex;
    
    vec4 normal = vec4(gl_Normal, 0);
    normal = normalize(inverseWorld * normal);
 
    vec4 worldPosition = gl_Vertex * transposedWorld;
    vec4 lightVector = worldPosition - vec4(lightPosition, 1.0);
    lightVector = normalize(lightVector);
    
    float dotProd = dot(-lightVector, normal);
    vec4 color = vec4(.7, .7, .7, 1) * dotProd;
    gl_FrontColor = gl_BackColor = vec4(color.x, color.y, color.z, 0);
}
