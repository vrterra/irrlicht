#version 330 compatibility

uniform mat4 worldViewProj;

out vec4 position;
out float dotProduct;

void main(void)
{
    position = gl_Vertex;
    gl_Position = worldViewProj * gl_Vertex;

    dotProduct = dot(normalize(gl_Normal), vec3(0,1,0));

    gl_FrontColor = gl_BackColor = vec4(1,1,1,0);
}
